### Display matching items from directory tree

    Usage: ft DOTKIND1 FTYPE from [DOTKIND2] DEPTH DIRECTORY [avoid SUBDIRNAME [SUBDIRNAME ...]]
    DOTKIND1:
       any | dot | nodot
    FTYPE:
       dir | file | exec | thing - means \"anything\"
    DOTKIND2 (default: any):
       any | dot | nodot
    DEPTH (starts from 0):
       [min-depth]:[max-depth]
    SUBDIRNAME - a file name of a subdirectory
    Hint:
        The sequence of arguments can be shortened from right side to any sensible lenth
        Any argument requires all of the essential predecessors to be provided
