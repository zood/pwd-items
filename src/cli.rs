use core::ops::RangeInclusive;
use std::{error::Error, ffi::OsStr, os::unix::ffi::OsStrExt, path::PathBuf};

use crate::filter::{Dot, Filter, Ftype};

pub struct Args<'a> {
	pub dir: PathBuf,
	pub filter: Filter<'a>,
}

pub fn parse_args<'a>() -> Result<Args<'a>, Box<dyn Error>> {
	let mut args = std::env::args_os().skip(1).peekable();

	let dotwhat: Dot = match args.next() {
		None => Dot::default(),
		Some(arg) => arg.to_string_lossy().as_ref().try_into()?,
	};

	let ftype: Ftype = match args.next() {
		None => Ftype::default(),
		Some(arg) => arg.to_string_lossy().as_ref().try_into()?,
	};

	if args.next().is_some_and(|arg| arg != "from") {
		return Err("expected placeholder word: `from`".into());
	}

	let dotwhere: Dot = if args
		.peek()
		.is_some_and(|a| a.to_string_lossy().contains(':'))
	{
		Dot::Any
	} else {
		match args.next() {
			None => Dot::default(),
			Some(arg) => arg.to_string_lossy().as_ref().try_into()?,
		}
	};

	let depth: RangeInclusive<usize> = match args.next() {
		None => RangeInclusive::new(0, usize::MAX),
		Some(arg) => {
			let arg = arg.to_string_lossy();
			if !&arg.contains(':') {
				return Err("invalid DEPTH specifier".into());
			}
			let mut iter = arg.split(':');
			let start = iter
				.next()
				.map(|arg| arg.parse::<usize>().unwrap_or(0))
				.unwrap();
			let end = iter
				.next()
				.map(|arg| arg.parse::<usize>().unwrap_or(usize::MAX))
				.unwrap();
			RangeInclusive::new(start, end)
		}
	};

	let dir = match args.next() {
		Some(arg) => {
			let dir = PathBuf::from(arg);
			if !dir.is_dir() {
				return Err(format!("invalid directory: '{}'", dir.display()).into());
			}
			dir
		}
		None => PathBuf::from("."),
	};

	let mut avoid = Vec::new();
	match args.next() {
		Some(arg) if arg == "avoid" => {
			avoid = args
				.map(|arg| {
					let no_prefix = arg.as_bytes().strip_prefix(b"./").unwrap_or(arg.as_bytes());
					OsStr::from_bytes(no_prefix.strip_suffix(b"/").unwrap_or(no_prefix)).to_owned()
				})
				.collect();
			if avoid.is_empty() {
				return Err("no avoided DIRNAME(s) provided".into());
			}
		}
		Some(arg) => return Err(format!("unexpected argument: {}", arg.to_string_lossy()).into()),
		None => (),
	};

	let filter: Filter = Filter::new(dotwhat, ftype, dotwhere, depth, avoid);

	Ok(Args { dir, filter })
}
