use std::{ffi::OsString, fs::DirEntry};

use crate::test::{self, Avoid};

pub type Test<'a> = &'a dyn Fn(&DirEntry) -> bool;

const ANY: Test = &|_| true;

pub struct Filter<'a> {
	pub ftype: Ftype,
	pub depth: std::ops::RangeInclusive<usize>,
	pub dotwhat_tests: [Test<'a>; 2],
	pub dotwhere_test: Test<'a>,
	avoid: Vec<OsString>,
}

impl Filter<'_> {
	pub fn new(
		dotwhat: Dot,
		ftype: Ftype,
		dotwhere: Dot,
		depth: std::ops::RangeInclusive<usize>,
		avoid: Vec<OsString>,
	) -> Self {
		Self {
			ftype,
			depth,
			dotwhat_tests: [dotwhat.to_test(), ftype.to_test()],
			dotwhere_test: dotwhere.to_test(),
			avoid,
		}
	}

	pub fn avoids<P: Avoid>(&self, item: P) -> bool {
		item.is_avoided_by(&self.avoid)
	}
}

#[derive(Copy, Clone, PartialEq, Default)]
pub enum Dot {
	#[default]
	Any,
	No,
	Only,
}

impl Dot {
	pub fn to_test<'a>(self) -> Test<'a> {
		match self {
			Dot::Any => &ANY,
			Dot::No => &test::is_visible,
			Dot::Only => &test::is_hidden,
		}
	}
}

impl TryFrom<&str> for Dot {
	type Error = String;
	fn try_from(s: &str) -> Result<Self, Self::Error> {
		match s {
			"any" => Ok(Dot::Any),
			"nodot" => Ok(Dot::No),
			"dot" => Ok(Dot::Only),
			_ => Err(format!("expected DOT specifier, found: {s}")),
		}
	}
}

#[derive(Copy, Clone, PartialEq, Default)]
pub enum Ftype {
	#[default]
	Any,
	Dir,
	File,
	Exec,
}

impl Ftype {
	pub fn to_test<'a>(self) -> Test<'a> {
		match self {
			Ftype::Any => &ANY,
			Ftype::Dir => &test::is_dir_or_symlink_to_dir,
			Ftype::File => &test::is_not_a_dir,
			Ftype::Exec => &test::is_exec_file_or_symlink_to_exec_file,
		}
	}
}

impl TryFrom<&str> for Ftype {
	type Error = String;
	fn try_from(s: &str) -> Result<Self, Self::Error> {
		match s {
			"thing" => Ok(Ftype::Any),
			"dir" => Ok(Ftype::Dir),
			"file" => Ok(Ftype::File),
			"exec" => Ok(Ftype::Exec),
			_ => Err(format!("expected FTYPE specifier, found: {s}")),
		}
	}
}
