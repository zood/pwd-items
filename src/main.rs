#![feature(dir_entry_ext2)]

use std::{
	borrow::Cow,
	fs::DirEntry,
	io::{self, BufWriter, Stdout, Write},
	os::unix::prelude::OsStrExt,
	path::Path,
};

mod cli;
mod filter;
use filter::{Filter, Ftype};
mod test;
use test::DirExt;

type BW<'a> = &'a mut BufWriter<Stdout>;

struct App;
impl App {
	const HELP: &str = "\
About: display matching items from directory tree
Usage: ft DOTKIND1 FTYPE from [DOTKIND2] DEPTH DIRECTORY [avoid SUBDIRNAME [SUBDIRNAME ...]]
DOTKIND1:
   any | dot | nodot
FTYPE:
   dir | file | exec | thing - means \"anything\"
DOTKIND2 (default: any):
   any | dot | nodot
DEPTH (starts from 0):
   [min-depth]:[max-depth]
SUBDIRNAME - a file name of a subdirectory
Hint:
    The sequence of arguments can be shortened from right side to any sensible lenth
    Any argument requires all of the essential predecessors to be provided";
	fn die(self) -> ! {
		std::process::exit(1);
	}
	fn help(self) -> Self {
		eprintln!("{}", Self::HELP);
		self
	}
	fn err(self, msg: impl std::fmt::Display) -> Self {
		eprintln!("[ft] ERROR: {msg}\n");
		self
	}
}

fn output(entry: &DirEntry, writer: BW, ftype: Ftype) -> io::Result<()> {
	let path = entry.path();
	let path = path.strip_prefix("./").unwrap_or(&path);
	let path = match ftype {
		Ftype::Dir => Cow::from(path.join("")), // add a trailing slash
		Ftype::Any if entry.is_dir_or_symlink_to_dir() => Cow::from(path.join("")), // add a trailing slash
		_ => Cow::from(path),
	};
	writer.write_all(path.as_os_str().as_bytes())?;
	writer.write_all(b"\n")?;
	Ok(())
}

fn worker(dir: &Path, filter: &Filter, writer: BW, c: usize) -> io::Result<()> {
	if filter.avoids(dir) {
		return Ok(())
	}

	let mut dirs_to_visit = Vec::new();
	if filter.depth.end() >= &c {
		for entry in path_iterator(dir) {
			if entry.is_dir() && (filter.dotwhere_test)(&entry) {
				dirs_to_visit.push(entry.path());
			}
			if filter.depth.contains(&c)
				&& !filter.avoids(&entry)
				&& filter.dotwhat_tests.iter().all(|test| (test)(&entry))
			{
				output(&entry, writer, filter.ftype)?;
			}
		}
	}
	dirs_to_visit
		.iter()
		.try_for_each(|dir| worker(dir, filter, writer, c + 1))?;
	Ok(())
}

fn path_iterator(path: &Path) -> Box<dyn Iterator<Item = DirEntry>> {
	match path.read_dir() {
		Ok(reader) => Box::new(reader.flatten()),
		Err(_) => Box::new(std::iter::empty::<DirEntry>()),
	}
}

fn main() {
	let cli::Args { dir, filter } = cli::parse_args().unwrap_or_else(|e| App.err(e).help().die());
	let mut writer = BufWriter::new(io::stdout());
	worker(&dir, &filter, &mut writer, 0_usize).unwrap();
}
