use std::{
	ffi::OsString,
	fs::DirEntry,
	os::unix::{ffi::OsStrExt, fs::DirEntryExt2},
	path::Path,
};

use is_executable::IsExecutable;

pub fn is_dir_or_symlink_to_dir(entry: &DirEntry) -> bool {
	entry.is_dir_or_symlink_to_dir()
}

pub fn is_not_a_dir(entry: &DirEntry) -> bool {
	entry.is_not_a_dir()
}

pub fn is_hidden(entry: &DirEntry) -> bool {
	entry.is_hidden()
}

pub fn is_visible(entry: &DirEntry) -> bool {
	entry.is_visible()
}

pub fn is_exec_file_or_symlink_to_exec_file(entry: &DirEntry) -> bool {
	entry.is_exec_file_or_symlink_to_exec_file()
}

pub trait DirExt {
	fn is_dir(&self) -> bool;
	fn is_dir_or_symlink_to_dir(&self) -> bool;
	fn is_not_a_dir(&self) -> bool;
	fn is_hidden(&self) -> bool;
	fn is_visible(&self) -> bool;
	fn is_exec_file_or_symlink_to_exec_file(&self) -> bool;
}

impl DirExt for DirEntry {
	fn is_dir(&self) -> bool {
		self.file_type().is_ok_and(|p| p.is_dir())
	}
	fn is_dir_or_symlink_to_dir(&self) -> bool {
		self.is_dir() || self.path().is_dir()
	}
	fn is_not_a_dir(&self) -> bool {
		let Ok(ft) = self.file_type() else { return false };
		!ft.is_dir()
	}
	fn is_hidden(&self) -> bool {
		self.file_name_ref().as_bytes().first() == Some(&b'.')
	}
	fn is_visible(&self) -> bool {
		!self.is_hidden()
	}
	fn is_exec_file_or_symlink_to_exec_file(&self) -> bool {
		let path = self.path();
		path.is_file() && path.is_executable()
	}
}

pub trait Avoid {
	fn is_avoided_by(&self, avoid: &[OsString]) -> bool;
}

impl Avoid for &Path {
	fn is_avoided_by(&self, avoid: &[OsString]) -> bool {
		let Some(fname) = self.file_name() else { return false };
		avoid.iter().any(|avo| avo == fname)
	}
}

impl Avoid for &DirEntry {
	fn is_avoided_by(&self, avoid: &[OsString]) -> bool {
		avoid.iter().any(|avo| avo == &self.file_name())
	}
}
